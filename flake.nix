{
  description = "A bespoke block device provisioning utility";

  inputs =
    {
      fu.url = "github:numtide/flake-utils";
    };

  outputs = { self, nixpkgs, fu }:
    fu.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (
        system:
        let
          packageName = "fiddy";
          version = "0.11.0"; # %AUTOINCREMENTED%

          pkgs = nixpkgs.legacyPackages.${system};
          deps = with pkgs;
            [
              btrfs-progs
              coreutils
              cryptsetup
              diffutils
              dosfstools
              findutils
              gawk
              gnugrep
              gnused
              gnutar
              gptfdisk
              lvm2
              nvme-cli
              openssl
              pbkdf2-sha512
              perl
              procps
              util-linux
              yubikey-personalization
            ];
          path = pkgs.lib.makeBinPath deps;

          pbkdf2-sha512 = pkgs.stdenv.mkDerivation
            rec
            {
              name = "pbkdf2-sha512";
              version = "latest";
              src = pkgs.fetchurl
                {
                  url =
                    "https://raw.githubusercontent.com/NixOS/nixpkgs/master/nixos/modules/system/boot/pbkdf2-sha512.c";
                  sha256 =
                    "0ky414spzpndiifk7wca3q3l9gzs1ksn763dmy48xdn3q0i75s9r";
                };

              buildInputs = [ pkgs.openssl ];

              unpackPhase = ":";
              buildPhase =
                ''
                  cc -O3 -I${pkgs.openssl.dev}/include \
                    -L${pkgs.openssl.out}/lib ${src} -o pbkdf2-sha512 -lcrypto
                '';
              installPhase =
                ''
                  mkdir -p $out/bin
                  install -m755 pbkdf2-sha512 $out/bin/pbkdf2-sha512
                '';
            };
        in
        rec
        {
          packages.${packageName} = pkgs.stdenv.mkDerivation
            {
              name = "fiddy";
              inherit version;
              src = self;

              buildInputs = [ pkgs.makeWrapper ];

              patchPhase =
                ''
                  # Hardcode program name to prevent display of wrapper file
                  #+ name in program output
                  substituteInPlace fiddy --replace \
                    '_PROGRAM_NAME="$(basename "${"\${0}"}")"' \
                    '_PROGRAM_NAME=fiddy'
                '';
              dontBuild = true;
              installPhase =
                ''
                  mkdir -p $out/bin
                  install -Dm 0755 fiddy $out/bin/
                  wrapProgram "$out/bin/fiddy" --prefix PATH : "${path}"
                '';
            };

          defaultPackage = self.packages.${system}.${packageName};
          apps.${packageName} = fu.lib.mkApp
            {
              drv = packages.${packageName};
            };
          defaultApp = apps.${packageName};
          devShell = pkgs.mkShell { buildInputs = deps; };
        }
      );
}
