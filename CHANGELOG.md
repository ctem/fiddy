# Changelog

All notable changes to [`fiddy`] are documented in this file.

The format is based on [Keep a Changelog] with entry subheadings ordered as follows: `Packaging`, `Added`, `Changed`, `Fixed`, `Removed`.

[`fiddy`] adheres to [Semantic Versioning].

[`fiddy`]: https://codeberg.org/ctem/fiddy
[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html

## [0.11.0](https://codeberg.org/ctem/fiddy/compare/v0.11.0...v0.10.0) - 2022-05-01

### Packaging

- **Flake:** bump lock file
- **README:**
  - List subcommand `guide`
  - Add supplementary content:
    - Program and subcommand descriptions
    - Additional features and acknowledgments
    - `Examples`, `Limitations`, and `Warnings` sections

### Added

- Subcommand `guide`
- Help content for `guide`
- Public repository URL

## [0.10.0](https://codeberg.org/ctem/fiddy/compare/v0.10.0...v0.9.0) - 2021-11-21

### Packaging

- **Flake:** add lock file
- **README:** list subcommand `env`

### Added

- Subcommand `env`
- Help content for `env`

## [0.9.0](https://codeberg.org/ctem/fiddy/compare/v0.9.0...v0.8.0) - 2021-10-17

### Packaging

- **README:** list nested subcommand `header restore`

### Added

- Nested subcommand `header restore`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
    - Archive
  - Resolution:
    - Paths
    - Names
    - Encryption hardware
  - Setup:
    - Ramfs
    - Archive extraction and verification
    - Support data verification
  - LUKS header and support data restoration
  - Cleanup
- Help content for `header restore`

## [0.8.0](https://codeberg.org/ctem/fiddy/compare/v0.8.0...v0.7.0) - 2021-10-03

### Packaging

- **README:** list nested subcommand `header destroy`

### Added

- Nested subcommand `header destroy`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
    - LUKS container state
  - Resolution:
    - Paths
    - Names
    - Encryption hardware
  - Confirmation
  - LUKS header destruction and support data deletion
  - Cleanup
- Help content for `header destroy`

## [0.7.0](https://codeberg.org/ctem/fiddy/compare/v0.7.0...v0.6.0) - 2021-09-19

### Packaging

- **README:** list nested subcommand `header backup`

### Added

- Nested subcommand `header backup`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
    - Archive
  - Resolution:
    - Paths
    - Names
  - Setup:
    - LUKS encryption hardware
    - Ramfs
    - Support data retrieval
  - Backup operations:
    - LUKS header retrieval
    - Archive creation and compression
  - Cleanup
- Help content for `header backup`

## [0.6.0](https://codeberg.org/ctem/fiddy/compare/v0.6.0...v0.5.0) - 2021-08-22

### Packaging

- **README:** list subcommand `close`

### Added

- Subcommand `close`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
  - Resolution:
    - Paths
    - Names
    - Mount point(s)
    - VG(s)
  - Close operations:
    - Unmounting
    - Swap LV disabling
    - VG deactivation
    - LUKS container deactivation'
- Help content for `close`

## [0.5.0](https://codeberg.org/ctem/fiddy/compare/v0.5.0...v0.4.0) - 2021-08-08

### Packaging

- **Flake:** add development shell dependencies corresponding to latest updates
- **README:** list subcommand `open`

### Added

- Subcommand `open`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
  - Resolution, including prompting:
    - Names
    - Mount point(s)
  - Setup:
    - LUKS encryption hardware
    - Ramfs and support partition
    - Archive (to be generated with upcoming nested subcommmand `header backup`)
  - LUKS container activation and new LUKS key generation
  - LVM operations:
    - VG activation
    - Primary LV mounting
    - Swap LV enabling
  - Cleanup
- Help content for `open`

## [0.4.0](https://codeberg.org/ctem/fiddy/compare/v0.4.0...v0.3.0) - 2021-07-11

### Packaging

- Add Nix flake, including development shell dependencies corresponding to latest updates
- Add configuration for nix-direnv
- **README:** list subcommand `format` and add Nix flake to feature list

### Added

- Subcommand `format`, including the following enhancements:
  - Validation:
    - Parameters
    - Environment
    - Target device
    - LUKS partition setup
    - Support partition name
    - Swap LV setup
    - Salt file path
  - Resolution:
    - Target device, including prompting
    - Partition numbers
    - Names
    - Primary LV subvolumes
    - Encryption hardware
    - Support mount point
    - LUKS passphrase
  - LUKS encryption hardware initialization and key generation
  - Target device erasure
  - Partition-level operations:
    - Partitioning
    - Label assignment
  - Formatting:
    - Support partition
    - LUKS partition
    - Primary LV, including subvolume creation
  - LVM setup on LUKS partition
  - Cleanup
- Help content for `format`

## [0.3.0](https://codeberg.org/ctem/fiddy/compare/v0.3.0...v0.2.0) - 2021-05-16

### Packaging

- **README:** list subcommand `header` and nested subcommand `header subcommands`

### Added

- Subcommand `header`
- Nested subcommand `header subcommands`
- Help content for `header` and `header subcommands`
- Outline of upcoming program-specific subcommands

## [0.2.0](https://codeberg.org/ctem/fiddy/compare/v0.2.0...v0.1.0) - 2021-05-02

### Packaging

- **README:** list subcommand `help`

### Added

- Subcommand `help`
- Global option `--help`
- Default help content
- Help content for `version`, `subcommands`, and `help`
- Resolution of available help content

## [0.1.0](https://codeberg.org/ctem/fiddy/compare/v0.1.0...v0.0.0) - 2021-04-18

### Packaging

- **License:** add a copy of GNU AGPLv3
- **README:** add preliminary documentation, listing subcommands `subcommands` and `version`

### Added

The initial `fiddy` script is introduced with an outline of the general program structure, as well as the following enhancements:

- Subcommands `subcommands` and `version`
- Dialog toggle
- Template-based output format handling, including handling of debug and exit output
- Strict mode
- Normalization of options
- Resolution of available subcommands
- Validation:
  - Bash version
  - General external dependencies
  - Environment variables
  - Specified subcommand

## [0.0.0](https://codeberg.org/ctem/fiddy/releases/tag/v0.0.0) - 2021-03-14

### Packaging

- Initialize empty repository
