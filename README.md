> _“When 49 ways to encrypt your drive simply won’t do, accept no substitutes.”_

# `fiddy`

Belonging to the high-level workflow optimization CLI category of GNU/Linux system administration tools, `fiddy` is a Bash script wrapping various programs to streamline block device provisioning for common use cases.

Support for full disk encryption being a _key_ feature (no pun intended), the name “fiddy” is a colloquial respelling of its acronym (FDE).

## Features

- (En/de)cryption with passphrase and/or encryption hardware
- Supported hardware-based encryption types:
  - HMAC-SHA1 challenge-response (e.g., YubiKey OTP)
  - [TODO] GPG smart card (e.g., Librem Key, Nitrokey, YubiKey)
- Special accommodation for secure erasure of NVMe SSDs
- Automatic logical volume management
- Optional swap management
- Optional friendly user dialogs to prompt for unspecified values and provide helpful hints (enabled by default)
- Strives for device decryption feature parity with [NixOS boot stage 1][luksroot]
- Single Bash script = portable
- Nix flake = hassle-free development environment (among other benefits)
- GNU AGPLv3 = free(d) software

## Overview

Subcommands:

- `close`
- `env`
- `format`
- `guide`
- `header`
  - `backup`
  - `destroy`
  - `restore`
  - `subcommands`
- `help`
- `open`
- `subcommands`
- `version`

By default, `fiddy format` will produce the following structure on each targeted device, which can be mounted and unmounted accordingly using `fiddy open` and `fiddy close`, respectively:

- GUID Partition Table (GPT)
  - Optional unencrypted, VFAT-formatted support partition using specified capacity
  - Linux Unified Key Setup (LUKS) (version 1 or 2)-initialized primary partition using remaining capacity
    - Logical Volume Manager (LVM) volume group (VG) with single physical volume (PV)
      - Optional swap logical volume (LV) using specified capacity
      - Btrfs-formatted primary LV using remaining capacity
        - Optional subvolume(s) based on template(s)

The LUKS header and key slot area, along with any encryption hardware-dependent support data, of each target device can be backed up to an archive using `fiddy header backup`, destroyed using `fiddy header destroy`, and restored from a backed up archive using `fiddy header restore`.

In addition to boilerplate commands `fiddy help`, `fiddy version`, and `fiddy [header] subcommands`, `fiddy` seeks to ease onboarding by providing `fiddy guide`, an interactive guide detailing a generic use case procedure with additional notes and tips. Also included is `fiddy env`, which outputs a complete listing of environment variables (with optional descriptions) that may be used to configure `fiddy`.

## Examples

By default, all arguments are optional as a prompt will appear for any missing values. However, executing `fiddy` commands with options is generally more convenient. The following examples showcase some commonly used options.

- Securely erase NVMe device “/dev/nvme0n1” and set it up for decryption with HMAC-SHA1 challenge-response mode-enabled hardware (such as a YubiKey with a properly configured OTP slot), using the name “mixtape” to generate partition and file system labels:

  ```bash
  fiddy format -H chalresp -n mixtape -s /dev/nvme0n1
  ```

- Set up (reformat) _existing_ HDD partition “/dev/sda2” (i.e., without touching the partition table) for passphrase-only decryption, using the name “root” to generate partition and file system labels:

  ```bash
  FIDDY_CRYPT_PART_NUM=2 fiddy format -H none -n root --no-partitioning /dev/sda
  ```

## Limitations

Please note the following limitations as of the current implementation:

- Unencrypted support partition formatting is limited to VFAT.
- Partitioning of unencrypted primary partitions is not implemented.
- LVM setup is baked into the primary partition formatting operation.
- Primary LV formatting is limited to Btrfs.

## Warnings

While this software is distributed for the benefit of all free folk, please be aware that it:

- is alpha-quality and does not yet include a test suite.
- performs destructive operations. Please create backups and use responsibly.
- has not been audited by a security professional. Please use in accordance with your threat model.

## Acknowledgments

- Hi, Mom!
- As a humble wrapper script, `fiddy` has a veritable hype crew of external programs to thank for all the heavy lifting. (Please refer to the [flake] dependencies for the package list.)
- `fiddy` has grown in the warmth and immutable luminance of the [Nix] community. In particular, such projects as [nixos-yubikey-luks] and its references—not to mention the NixOS [luksroot] module itself—informed procedures within `fiddy format` and `fiddy open`.
- `fiddy` was structurally informed by such prior art as the excellent [Bash Boilerplate] templates and their references.
- GParted im*parted* a helpful [reference][gparted] for maximum label lengths of various file systems.

## Contributing

Contributions are welcome!

`fiddy` is licensed under the GNU Affero General Public License v3.0 only (SPDX ID `AGPL-3.0-only`). A copy is included in the [COPYING] file.

Documentation in this repository is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International][cc] license (SPDX ID `CC-BY-SA-4.0`).

(☮️ + ❤️) / 🌐

[luksroot]: https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/system/boot/luksroot.nix
[flake]: flake.nix
[nix]: https://nixos.org/
[nixos-yubikey-luks]: https://github.com/sgillespie/nixos-yubikey-luks
[bash boilerplate]: https://github.com/xwmx/bash-boilerplate/
[gparted]: https://gitlab.gnome.org/GNOME/gparted/-/commit/ecb1f57594e8aebb60fda3e0160921b83750c54f
[copying]: COPYING
[cc]: https://creativecommons.org/licenses/by-sa/4.0/
